/**
 * Client side code.
 */

 //This is just a simple function to say it is a Registration App  

 (function() { //this is an IIFE
    var app = angular.module("RegApp", []); //angular always start with the registraion of the module. need to define a name in this case it is RegApp
    app.controller("RegistrationCtrl", ["$http", RegistrationCtrl]); //i name this controller as RegistrationCtrl

    function RegistrationCtrl($http) { //inject the component
        var self = this; // vm

        self.user = { //creating an object call email
            email: "",
            password: "",
            confirmpassword:"",
            name:"",
            gender:"",
            dateOfBirth:"",
            address:"",
            nationality:"",
            contactnumber:""
        };

        self.displayUser = {
            email: "",
            password: "",
            confirmpassword:"",
            name:"",
            gender:"",
            dateOfBirth:"",
            address:"",
            nationality:"",
            contactnumber:""
        };

        self.registerUser = function() {
            console.log(self.user.email);
            console.log(self.user.password);
            console.log(self.user.confirmpassword);
            console.log(self.user.name);
            console.log(self.user.gender);
            console.log(self.user.dateOfBirth);
            console.log(self.user.address);
            console.log(self.user.nationality);
            console.log(self.user.contactnumber);
            $http.post("/users", self.user)
                .then(function(result) {
                    console.log(result);
                    self.displayUser.email = result.data.email;
                    self.displayUser.password = result.data.password;
                    self.displayUser.password = result.data.confirmpassword;
                    self.displayUser.name = result.data.name;
                    self.displayUser.gender = result.data.gender;
                    self.displayUser.dateOfBirth = result.data.dateOfBirth;
                    self.displayUser.address = result.data.address;
                    self.displayUser.nationality = result.data.nationality;
                    self.displayUser.contactnumber = result.data.contactnumber;
                }).catch(function(e) {
                    console.log(e);
                });
        };
    }

})();