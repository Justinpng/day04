## PREPARE PROJECT STRUCTURE

1. CREATE MASTER DIRECTORY: mkdir (project name)
1. CREATE SERVER DIRECTORY: mkdir (project name>/server
1. CREATE CLIENT FOLDER: mkdir(project name)/client

## CREATE REPOSITORY ON BITBUCKET
1. go to bitbucket.com
1. on Dashboard, click repository and create new
1. assign a repository name, uncheck private
1. COPY REMOTE LINK FOR ORIGIN MASTER: 
    $ git remote add origin https://Justinpng@bitbucket.org/Justinpng/NEWFILENAME.git

## PREPARE GIT 
1. INTITATE GIT DIRECTORY: git init
1. ADD REOMOTE ORIGIN: $ git remote add origin https://Justinpng@bitbucket.org/Justinpng/day1.git
1. CHECK REMOTE ORIGIN STATUS: $ git remote -v
1. ADD FILE TO STAGING: $ git add .
1. COMMIT FILE TO LOCAL REPOSITORY: $ git commit -m "Description"
1. CHECK GIT STATUS: $ git status
1. PUSH TO ORIGIN MASTER: $ git push origin master
1. SWITCH TO BRANCH LEVEL:  $ git checkout -b feature_1

## GLOBAL UTILITY INSTALLATION
1. CHECK NPM STATUS: $ npm --version
1. INSTALL NODEMON: npm install -g nodemon
1. INSTALL NODEMON: npm install -g bower

## PREPARE EXPRESS JS (.JSON file)
1. INITITATE NPM: $ npm init
    name: (day01)
    version: (1.0.0)
    description: Key in description
    entry point: (index.js) server/app.js
    test command: git clone "directory"
    git repository: (bitbucket repository)
    keywords:
    author: Justin
    license: (ISC)
1. INSTALL JS EXPRESS: npm install express --save

## STARTING NODEMON (Root folder)
1. SETTING PORT (GOOD PRACTICE IF YOU NEED TO TEST ON SEVERAL APPLICATIONS)
    1. $ export NODE_PORT=3001 (e.g)
1. RUN NODEMON: nodemon


## ASSIGNING PORTS
1. CREATE A JS FILE(app.js) FOR LOCAL PORT ASSIGNENT

    console.log("Day Two Exercises");
    var express = require("express");
    var app = express();
    console.log(__dirname);
    console.log(__dirname + "/../");
    const NODE_PORT = process.env.NODE_PORT || 3000;

    app.use(express.static(__dirname + "/../client/"));
    app.listen(NODE_PORT,function(){
    console.log("Web App Started at localhost:" + NODE_PORT);
    });



## RUNNING THE WEB APP
    1. CREATE A "CLIENT" FOLDER AT /DayXX DIRECTORY
    1. CREATE A HTML FILE WITHIN THE CLIENT FOLDER
    1. VIEW THE HTML FILE USING A BROWSER (CHROME) WITH ADDRESS: http://localhost:3000/

## CREATE BOWER COMPONENTS (GLOBAL DIRECTORY)
1. CREATE .bowerrc IN WORKING DIRECTORY
1. {"directory": "client/bower_componets"}
1. INITATE BOWER COMPONENTS: $ npm init bower
1. CREATE DEPENDANCY IN THE BOWER FILE: $ bower install angular --save
1. CREATE DEPENDANCY IN BOOTSTRAP: $ bower install bootstrap --save
1. INSTALL FONT AWESOME: bower install font-awesome --save
1. INSTALL BODY PARSER 


## MISCELLANOUS KNOWLEDGE
1. HOW TO KNOW IF IT'S SERVER SIDE OR CLIENT SIDE CODE (INDICATE CLEARLY WITH /*COMMENT TAG)
    1. SERVER SIDE - Will contain the code "Require"
    1. CLIENT SIDE - Will contain the code "Function"
1. All "get.use" function has to be put before "app.use". Else, it might not be executed.
1. "app.listen" function is used to bring up/start the server
1. If you need to display anything from the controller, you will need to have double {{}}
